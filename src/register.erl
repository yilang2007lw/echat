-module (register).
-author ("Long Wei").

-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("basedir.hrl").

main() -> #template { file=?BASEDIR ++ "/templates/bare.html" }.

title() -> "Welcome to register xmpp client".

body() ->
    wf:wire(register, #event {
            type=click,
            postback=register
    }),

    wf:wire(register, username, #validate { validators=[
            #is_required { text="Username Required." }
    ]}),

    wf:wire(register, email, #validate { validators=[
            #is_required { text="Email Required." },
            #is_email { text="Email address invalid." }
    ]}),

    wf:wire(register, password, #validate { validators=[
            #is_required { text="Password Required." },
            #min_length { length=6, text="Password must be at least 6 characters long."}
    ]}),

    wf:wire(register, confirm_password, #validate { validators=[
            #is_required { text="Confirm password Required." },
            #confirm_password { password=password, text="Passwords must match." }
    ]}),

    [
        #h1 { text="Welcome to register xmpp account" },

        #panel { id=login_panel, class="Login", body=[
            #panel { id=username_panel, body=[
                #label { text="Username:" },

                #textbox { id="username", text="Type username", next="email" }
            ]},

            #panel { id=email_panel, body=[
                #label { text="Email address:" },

                #textbox { id="email", next="password" }
            ]},

            #panel { id=password_panel, body=[
                #label { text="Password:" },

                #password { id="password", next="confirm_password" }
            ]},

            #panel { id=confirm_password_panel, body=[
                #label { text="Confirm Password:" },

                #password { id="confirm_password", next="login" }
            ]},

            #panel { id=button_panel, body=[

                #button { id="register", style="margin-left:10px;", text="Register" }
            ]}
        ]},

        #panel { id=tips }
    ].

event(register) ->
    %%?PRINT({event, now()}),
    wf:update(tips, [ #p { body="you clicked register." } ]),
    wf:redirect("/main").
