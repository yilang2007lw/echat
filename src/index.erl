-module (index).
-author ("Long Wei").

-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("basedir.hrl").

main() -> #template { file=?BASEDIR ++ "/templates/bare.html" }.

title() -> "Welcome to xmpp web im".

body() ->
    wf:wire(login, #event {
            type=click,
            postback=login
    }),

    wf:wire(register, #event {
            type=click,
            postback=register
    }),

    wf:wire(login, username, #validate { validators=[
            #is_required { text="Username Required." }
    ]}),

    wf:wire(login, password, #validate { validators=[
            #is_required { text="Password Required." }
    ]}),

    [
        #h1 { text="Welcome to xmpp web client!" },

        #panel { id=login_panel, class="Login", body=[
            #panel { id=username_panel, body=[
                #label { text="Username:" },
                
                #textbox { id="username", placeholder="Please input username", next="password" }
            ]},

            #panel { id=password_panel, body=[
                #label { text="Password:" },

                #password { id="password", next="login" }
            ]},

            #panel { id=button_panel, body=[
                #button { id="login", text="Login" },

                #button { id="register", style="margin-left:10px;", text="Register" }
            ]}
        ]},

        %%#panel { id=tips },
        #flash {}
    ].

event(login) ->
    %%?PRINT({event, now()}),
    %%wf:update(tips, [ #p { body="you clicked login." } ]);
    wf:flash("you clicked login."),
    wf:redirect("/main");
event(register) ->
    %%?PRINT({event, now()}),
    %%wf:update(tips, [ #p { body="you clicked register." } ]).
    wf:flash("you clicked register."),
    wf:redirect("/register").
