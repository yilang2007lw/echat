-module (main).
-author ("Long Wei").

-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("basedir.hrl").

main() -> #template { file=?BASEDIR ++ "/templates/bare.html" }.

title() -> "Welcome to xmpp web im".

body() ->
    [
        "<p>
        Welcome to xmpp web client!
        "
    ].
