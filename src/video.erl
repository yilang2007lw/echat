-module (video).
-author ("Long Wei").

-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("basedir.hrl").

main() -> #template { file=?BASEDIR ++ "/templates/bare.html" }.

title() -> "Test for HTML5 video".

body() ->
    [
        #h1 { text="Test for HTML5 video" }
    ].
